package com.inapp.question.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.inapp.question.model.Question;
import com.inapp.question.model.QuestionWrapper;
import com.inapp.question.model.Response;
import com.inapp.question.service.QuestionService;

@RestController
@RequestMapping("question")
public class QuestionController {
	@Autowired
	QuestionService questionService;

	@GetMapping("all")
	private ResponseEntity<List<Question>> getAllQuestions()
	{
		return questionService.getAllQuestions();
	}
	
	@GetMapping("category/{category}")
	private ResponseEntity<List<Question>> getQuestionsByCategory(@PathVariable String category)
	{
		return questionService.getQuestionsByCategory(category);
	}
	
	@PostMapping("add")
	private ResponseEntity<String> addNewQuestion(@RequestBody Question question)
	{
		return questionService.saveQuestion(question);
	}
	
	@GetMapping("generate")
	private ResponseEntity<List<Integer>> generateQuestionsByCategory(@RequestParam String category,@RequestParam int noOfQuestions)
	{
		return questionService.generateQuestionsbyCategory(category,noOfQuestions);
	}
	
	@PostMapping("get")
	private ResponseEntity<List<QuestionWrapper>> getQuestionsById(@RequestBody List<Integer> questionIdList)
	{
		return questionService.getQuestionsById(questionIdList);
	}
	
	@PostMapping("getscore")
	private ResponseEntity<Integer> getScore(@RequestBody List<Response> responses)
	{
		return questionService.getScore(responses);
	}
	
	
}

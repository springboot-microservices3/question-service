package com.inapp.question.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.inapp.question.model.Question;

public interface QuestionRepository  extends JpaRepository<Question, Integer>{

	List<Question> findAllByCodinglanguage(String category);
	
	@Query(value = "select q.id from Question q where codinglanguage =:language order by random() limit :limit")
	List<Integer> generateQuestions(String language, int limit);

}

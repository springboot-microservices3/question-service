package com.inapp.question.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.inapp.question.model.Question;
import com.inapp.question.model.QuestionWrapper;
import com.inapp.question.model.Response;
import com.inapp.question.repository.QuestionRepository;

@Service
public class QuestionService {
	@Autowired
	QuestionRepository questionRepo;

	public ResponseEntity<List<Question>> getAllQuestions() {
		try {
			List<Question> questions = questionRepo.findAll();
			return  new ResponseEntity<>(questions,HttpStatus.OK);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return new ResponseEntity<>(new ArrayList<>(),HttpStatus.NOT_FOUND);
	}

	public ResponseEntity<List<Question>> getQuestionsByCategory(String category) {
		try {
			List<Question> questions = questionRepo.findAllByCodinglanguage(category);
			return  new ResponseEntity<>(questions,HttpStatus.OK);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return new ResponseEntity<>(new ArrayList<>(),HttpStatus.NOT_FOUND);
	}

	public ResponseEntity<String> saveQuestion(Question question) {
		try {
			questionRepo.save(question);
			return  new ResponseEntity<>("New Question added successfully",HttpStatus.CREATED);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return new ResponseEntity<>("Error, please check server logs",HttpStatus.NOT_FOUND);
	}

	public ResponseEntity<List<Integer>> generateQuestionsbyCategory(String language, int limit) {
		try {
			List<Integer> questionsList = questionRepo.generateQuestions(language,limit);
			return  new ResponseEntity<>(questionsList,HttpStatus.OK);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return new ResponseEntity<>(new ArrayList<>(),HttpStatus.NOT_FOUND);
	}

	public ResponseEntity<List<QuestionWrapper>> getQuestionsById(List<Integer> questionIdList) {
		try {
			List<QuestionWrapper> questionWrapperList = questionIdList.stream()
				    .map(id -> convertToWrapper(questionRepo.findById(id).orElse(null)))
				    .filter(Objects::nonNull)
				    .collect(Collectors.toList());
			return new ResponseEntity<>(questionWrapperList, HttpStatus.OK);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return new ResponseEntity<>(new ArrayList<>(),HttpStatus.NOT_FOUND);
	}
	
	private QuestionWrapper convertToWrapper(Question question)
	{
		if (question == null) {
	        return null;
	    }
	    return new QuestionWrapper(
	    	question.getId(),
	        question.getQuestion(),
	        question.getOption1(),
	        question.getOption2(),
	        question.getOption3(),
	        question.getOption4()
	    );
	}

	public ResponseEntity<Integer> getScore(List<Response> responses) {
		int score = 0;
		try {
			for(Response response:responses)
			{
				Question question=questionRepo.findById(response.getId()).get();
				if(question.getCorrectanswer().equals(response.getResponse()))
				{
					score++;
				}
			}
			return new ResponseEntity<>(score,HttpStatus.OK);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
	}

}
